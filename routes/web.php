<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return ['apiVersion' => '0.0.1'];
});



Route::get('/db', function () {
    return App\User::all();
});


Route::get('/env', function () {
    return $_ENV;
});

Route::get('/health', function () {
    return 'health endpoint';
});


