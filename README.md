### Run on kubernetes cloud provider

##### Go to kubernetes folder
```sh
$ cd kubernetes
```

First you have to generate the value of these env variables to use on file config.secret-example.yml (Do not commit this changes)
NOTE (this app use a mysql sass service)

```yml
DB_HOST: 
DB_PORT: 
DB_DATABASE: 
DB_USERNAME:
DB_PASSWORD: 
```
example to generate base64 value
```sh
$ echo -n 'admin' | base64
```

##### Create secret config env variables
```sh
$ kubectl create -f config-secret-example.yaml
```



##### Create deployment
```sh
$ kubectl create -f deployment.yaml
```

##### Create service using Load balancer
```sh
$ kubectl create -f service.yaml
```

##### After a few minutes , you cloud provider will generate your public ip adress 

```sh
$ kubectl get svc
```

##### Or if you use minikube , get ip virtual machine
```sh
$ minikube ip
```

##### Install prometheus/grafana monitoring

First you have to install helm package
https://helm.sh/docs/intro/quickstart/

And run this commads
```sh
$ kubectl create ns monitor
$ helm install prometheus-operator stable/prometheus-operator --namespace monitor
```

User port-forward to access grafana dashboard 
```sh
$ port-forward -n monitor yourgrafanapod 3000
```
Note that you should use "admin" as the login and "prom-operator" as the password





#### Run on local development

##### Clone repository

```sh
git clone git@gitlab.com:ernesto27/laravel-kubernetes.git
```

##### Create and run service
```sh
docker-compose -f docker/development/docker-compose.yml up -d
```

##### If you run container for first time you have to run this command
```sh
docker exec -it development_app_1 bash -c "composer install && cp .env.example .env && php artisan key:generate"
```

##### Update .env connection data

```
DB_CONNECTION=mysql
DB_HOST=
DB_PORT=
DB_DATABASE=
DB_USERNAME=
DB_PASSWORD=
```

URl local
http://localhost:8888/
